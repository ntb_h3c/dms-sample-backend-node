/**
 * Created by root on 7/3/17.
 */
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = require('./routes');

const app = express();
app.use(bodyParser.json());
app.use('/', router);
app.use(cors());

app.listen(8008, function(err){
    if(err){
        console.log('Server can not listen on port 8008')
        return;
    }
    console.log('Server listening on port 8008');
})
