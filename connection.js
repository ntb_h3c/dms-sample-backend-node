/**
 * Created by root on 7/3/17.
 */
const elastic_search = require('elasticsearch');

const client = new elastic_search.Client({
    hosts:[
        'http://localhost:9200'
    ]
});


module.exports = client;