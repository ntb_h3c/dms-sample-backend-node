/**
 * Created by root on 7/7/17.
 */
const express = require('express');
const client = require('./connection');

const router = express.Router();

//get all movies
router.get('/movies', function(req,res){
    client.search({
        index: 'movies',
        type: 'movie',

    }, function(error,response,status){
        if(error)
            res.status(500).json(error);
        else{
            res.status(200).json(response);
        }
    })
})

//get movies by country
router.get('/movies/country/:country', function(req,res){
    client.search({
        index: 'movies',
        type: 'movie',
        body:{
            query:{
                match:{
                    "releaseCountry":req.params.country
                }
            }
        }
    }, function(error,response,status){
        if(error)
            res.status(500).json(error);
        else
            res.status(200).json(response);
    })
})


//get movies by title
router.get('/movies/title/:title', function(req,res){
    client.search({
        index: 'movies',
        type: 'movie',
        body:{
            query:{
                match:{
                    "title":req.params.tite
                }
            }
        }
    }, function(error,response,status){
        if(error)
            res.status(500).json(error);
        else
            res.status(200).json(response);
    })
})

//get movies by imdb
router.get('/movies/imdb/:imdb', function(req,res){
    client.search({
        index: 'movies',
        type: 'movie',
        body:{
            query:{
                match:{
                    "imdbId":req.params.imdb
                }
            }
        }
    }, function(error,response,status){
        if(error)
            res.status(500).json(error);
        else
            res.status(200).json(response);
    })
})

//add new movies
router.post('/movies', function(req,res){
    //validation
    if(req.body.imdb==""||req.body.imdb==undefined)
        res.status(400).json({success:false, message:"IMDB Number is not set"});
    else if(req.body.title==""||req.body.title==undefined)
        res.status(400).json({success:false, message:"Title is not set"});
    else if(req.body.releaseCountry==""||req.body.releaseCountry==undefined)
        res.status(400).json({success:false, message:"Release Country is not set"});
    else if(req.body.releaseDate==""||req.body.releaseDate==undefined)
        res.status(400).json({success:false, message:"Release Date is not set"});
    else{
        client.index({
            index: 'movies',
            type: 'movie',
            body:{
                imdbId: req.body.imdb,
                title: req.body.title,
                releaseCountry: req.body.releaseCountry,
                releaseDate: req.body.releaseDate
            }
        }, function(error, response){
            if(error)
                res.status(500).send(error);
            else
                res.status(200).send(response);
        })
    }
})

//delete movie
router.delete('/movies/:id', function(req,res){
    client.delete({
        index: 'movies',
        type: 'movie',
        id: req.params.id
    }, function(error, response){
       if(error)
           res.status(500).json(error);
       else
           res.status(200).json(response);
    });
})

//update movie
router.put('/movies/:id', function(req,res){
    //validate data
    if(req.body.imdbId==""||req.body.imdbId==undefined)
        res.status(400).json({success:false, message:"IMDB Number is not set"});
    else if(req.body.title==""||req.body.title==undefined)
        res.status(400).json({success:false, message:"Title is not set"});
    else if(req.body.releaseCountry==""||req.body.releaseCountry==undefined)
        res.status(400).json({success:false, message:"Release Country is not set"});
    else if(req.body.releaseDate==""||req.body.releaseDate==undefined)
        res.status(400).json({success:false, message:"Release Date is not set"});
    else{
        client.index({
            index: 'movies',
            type: 'movie',
            id: req.params.id,
            body:{
                imdbId: req.body.imdbId,
                title: req.body.title,
                releaseCountry: req.body.releaseCountry,
                releaseDate: req.body.releaseDate
            }
        }, function(error, response){
            if(error)
                res.status(500).send(error);
            else
                res.status(200).send(response);
        })
    }
})



module.exports = router;